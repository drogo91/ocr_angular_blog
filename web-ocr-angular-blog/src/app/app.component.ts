import { Component } from '@angular/core';
import { Post } from "./shared/model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public title: string = 'web-ocr-angular-blog';
  public posts: Post[] = [{
    title: 'Manger des nouilles',
    content: 'jaime manger des nouilles',
    loveIts: 4,
    created_at: null,
  } as Post, {
    title: 'Le sport',
    content: 'jaime le sport',
    loveIts: 4,
    created_at: null,
  } as Post];

}
